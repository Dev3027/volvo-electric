This project is not currently being worked on.

# volvo-electric

Static site for Volvo electric cars built and modified from a free Udemy course on TailwindCss.

![screen shot](https://gitlab.com/Dev3027/volvo-electric/-/raw/main/images/volvo.png)

## Description
The free course to create this site can be found [here](https://www.udemy.com/share/105Pgi3@fLJeowilVIMdBKpV5OSB5nOt_sH-KY_TdbCYHh0fP8LBrtG_Js-JFZUQRQUb_Ks4/) by code bless you. I found it very enjoyable and fun. The site is HTML and CSS and is hosted here through Gitlab. If you want to clone and modify local then I suggest removing the yaml and gem files if they get cloned. They will need Jekyll and Ruby to work. If you would like to know more about those you can review information at this [gitlab doc](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html). You can preview the site [here](https://dev3027.gitlab.io/volvo-electric/)

## Usage
This project is an HTML template to be used by anyone. Make it yours and have fun.

## Authors and acknowledgment
Mason Roberts - Dev3027

